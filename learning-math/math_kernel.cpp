#include "math_kernel.h"





math_kernel::math_kernel( short dificulty)
{
    srand(time(nullptr));
    this->kernel_dificulty = dificulty;
    this->mode = "practice";
    this->corrects_question = 0;
}






QString math_kernel::get_operation()
{
    this->generate_pair();
    return QString::number(this->buffer[0]) + " X " + QString::number(this->buffer[1]);
}




int math_kernel::get_solution()
{
    return buffer[2];
}





int math_kernel::get_aleatory()
{
    return 1+rand()%4;
}




bool math_kernel::compare_solution(int user_solution)
{
    if (user_solution == buffer[2]){
        std::cout << "[good election]" << std::endl;
        evaluate_learn_mode(true);
        return true;

    }
    else {
        std::cout << "[bad  election]" << std::endl;
        evaluate_learn_mode(false);
        return false;
    }
}








void math_kernel::evaluate_learn_mode(bool acert)
{

    if(acert){
        if(mode=="practice" or mode=="timeout"){
            corrects_question++;
            if (corrects_question>10){
                kernel_dificulty++;
                corrects_question = 0;
            }
        }
    }
    else {
        if(mode=="practice"){
            corrects_question--;
        }
    }
}

QString math_kernel::getMode() const
{
    return mode;
}

void math_kernel::setMode(const QString &value)
{
    mode = value;
}










void math_kernel::generate_pair()
{
    this->buffer[0] = this-> kernel_dificulty ;
    this->buffer[1] = 1 + rand()% 10;
    this->buffer[2] = this->buffer[0] * this->buffer[1];
    std::cout <<"generate : "<< buffer[0] << buffer[1] << buffer[2] << std::endl;

}


