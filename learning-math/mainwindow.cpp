#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QTimer>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    motor = new math_kernel(1);
    restar_question = new QTimer();
    animation_timer = new QTimer();
    connect(restar_question,SIGNAL(timeout()),this,SLOT(generate_new_question()));
    connect(animation_timer,SIGNAL(timeout()),this,SLOT(animate_counter_bar()));
    set_disable_election(true);
    set_hiden_election(true);
    progres = 0;


}


MainWindow::~MainWindow()
{
    delete ui;
}



void MainWindow::set_operation_screen(QString operation)
{
    ui->operacion->setText(operation);
    ui->operacion->setStyleSheet("font-size:30pt; font-weight:600;");
    ui->solucion1->setText(QString::number(motor->get_solution()+motor->get_aleatory()));
    ui->solucion2->setText(QString::number(motor->get_solution()-motor->get_aleatory()));
    ui->solucion3->setText(QString::number(motor->get_solution()+motor->get_aleatory()));
    ui->solucion4->setText(QString::number(motor->get_solution()-motor->get_aleatory()));
    if (motor->get_aleatory() == 1) {
        ui->solucion1->setText(QString::number(motor->get_solution()));
    }
    else if(motor->get_aleatory()==2){
        ui->solucion2->setText(QString::number(motor->get_solution()));
    }
    else if(motor->get_aleatory()==3){
        ui->solucion3->setText(QString::number(motor->get_solution()));
    }
    else{
        ui->solucion4->setText(QString::number(motor->get_solution()));
    }
}







void MainWindow::set_disable_election(bool mode)
{
    ui->solucion1->setDisabled(mode);
    ui->solucion2->setDisabled(mode);
    ui->solucion3->setDisabled(mode);
    ui->solucion4->setDisabled(mode);
}




void MainWindow::set_hiden_election(bool mode)
{
    if (mode) {
        ui->solucion1->hide();
        ui->solucion2->hide();
        ui->solucion3->hide();
        ui->solucion4->hide();
        ui->progress->hide();
    }
    else{
        ui->solucion1->show();
        ui->solucion2->show();
        ui->solucion3->show();
        ui->solucion4->show();
        if(motor->getMode()=="timeout"){
            ui->progress->show();
        }
    }
}



void MainWindow::generate_new_question()
{
     set_disable_election(false);
     set_hiden_election(false);
     set_operation_screen(motor->get_operation());
     restar_question->stop();
     if (motor->getMode()=="timeout"){
         restar_question->start(5000);
         progres = 0;
     }
}

void MainWindow::animate_counter_bar()
{
    progres = progres + 5;
    if (progres<5000){
        ui->progress->setValue(progres);
    }
}








void MainWindow::set_correct_operation()
{
    ui->operacion->setText("MUY BIEN!");
    ui->operacion->setStyleSheet("font-size:20pt; font-weight:600;");
    set_disable_election(true);
    set_hiden_election(true);
    restar_question->start(1000);

}





void MainWindow::set_fail_operation()
{
    ui->operacion->setText("TE EQUIVOCASTE");
    ui->operacion->setStyleSheet("font-size:20pt; font-weight:600;color:red;");
    set_disable_election(true);
    set_hiden_election(false);
    restar_question->start(1000);
}







void MainWindow::on_solucion1_clicked()
{
    if (motor->compare_solution(ui->solucion1->text().toInt())){
        set_correct_operation();
    }
    else {
        set_fail_operation();
    }

}



void MainWindow::on_solucion2_clicked()
{
    if (motor->compare_solution(ui->solucion2->text().toInt())){
        set_correct_operation();
    }
    else {
        set_fail_operation();
    }
}

void MainWindow::on_solucion3_clicked()
{
    if (motor->compare_solution(ui->solucion3->text().toInt())){
        set_correct_operation();
    }
    else {
        set_fail_operation();
    }
}


void MainWindow::on_solucion4_clicked()
{
    if (motor->compare_solution(ui->solucion4->text().toInt())){
        set_correct_operation();
    }
    else {
        set_fail_operation();
    }
}







void MainWindow::on_freetime_clicked()
{
    generate_new_question();
    set_hiden_election(false);
    motor->setMode("practice");
    animation_timer->stop();
    ui->progress->hide();
}

void MainWindow::on_contrareloj_clicked()
{
    motor->setMode("timeout");
    set_hiden_election(true);
    restar_question->start(5000);
    animation_timer->start(5);
    ui->operacion->setText("Preparate!");
    ui->operacion->setStyleSheet("font-size:20pt; font-weight:600;");

}
