#ifndef MATH_KERNEL_H
#define MATH_KERNEL_H
#include <random>
#include <iostream>
#include <QString>

class math_kernel
{
public:
    math_kernel(short int dificulty);
    QString get_operation();
    int get_solution();
    int get_aleatory();
    bool compare_solution(int user_solution);
    QString getMode() const;
    void setMode(const QString &value);

private:
    short int buffer[3];
    short int kernel_dificulty;
    short int corrects_question;
    void evaluate_learn_mode(bool acert);
    QString mode;
    void generate_pair();



};


#endif // MATH_KERNEL_H
