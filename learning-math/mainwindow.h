#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "math_kernel.h"
#include <QPushButton>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_solucion1_clicked();

    void on_solucion2_clicked();

    void on_solucion3_clicked();

    void on_solucion4_clicked();

private:
    Ui::MainWindow *ui;
    math_kernel *motor;
    QTimer *restar_question;
    QTimer *animation_timer;
    short int progres;
    void set_correct_operation();
    void set_fail_operation();
    void set_operation_screen(QString operation );
    void set_disable_election(bool mode);
    void set_hiden_election(bool mode);
private slots:
    void generate_new_question();
    void animate_counter_bar();
    void on_freetime_clicked();
    void on_contrareloj_clicked();
};

#endif // MAINWINDOW_H
